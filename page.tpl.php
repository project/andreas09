<?php

$color = 'blue'; //set to blue, black, green, orange, purple, or red
global $base_url;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <style type="text/css" media="all">@import "<?php print $base_url . '/' . $directory . '/colorcss/' . $color ?>.css";</style>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
  <div id="container">
    <div id="sitename">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" id="logo" alt="<?php print t('Home') ?>" /></a><?php } ?>
      <?php if ($site_name) { ?><h1><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><h2><?php print $site_slogan ?></h2><?php } ?>
    </div>
    <div id="mainmenu">
      <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'primary_links')) ?><?php } ?>
    </div>
    <div id="wrap">
      <div id="leftside">
        <?php if ($sidebar_left) {
          print $sidebar_left;
        } ?>
      </div>
      <div id="rightside">
        <?php if ($sidebar_right) {
        print $sidebar_right;
        } ?>
      </div>
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><br /><?php } ?>
      <div id="content">
        <?php print $breadcrumb ?>
        <?php if ($title) { ?><h1><?php print $title ?></h1><?php } ?>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
      </div>
      <br class="clear" />
    </div>
  </div>
  <div id="footer"><?php if (strlen($footer_message) > 2) { print $footer_message . ' | '; } ?>Design by <a href="http://andreasviklund.com">Andreas Viklund</a>
  </div>
  <?php print $closure ?>
</body>
</html>